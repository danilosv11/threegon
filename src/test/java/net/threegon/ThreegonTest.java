package net.threegon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ThreegonTest {
    @Test(expected = IllegalArgumentException.class)
    public void testDLowerThanZeroOrAIsZero(){
        Threegon tg = new Threegon(0,1,1);
        assertNull(tg.getRoots());
    }

    @Test
    public void testDEqualsZero(){
        Threegon tg = new Threegon(1,2,1);
        assertEquals(tg.getRoots()[0],-1, 10E-9);
        assertEquals(tg.getRoots()[1],-1,10E-9);
    }

    @Test
    public void testDBiggerThanZero(){
        Threegon tg = new Threegon(1,2,-3);
        assertEquals(tg.getRoots()[0],1, 10E-9);
        assertEquals(tg.getRoots()[1],-3,10E-9);
    }
}
